# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from cms.models import Item,Fact,Quote,ItemCatogory,QuoteCategory



def home(request):
    
    mongon = '伊藤苑'
    
    return render_to_response('itoen/index.html',        # 使用するテンプレート
                              {'mongon': mongon},       # テンプレートに渡すデータ
                              context_instance=RequestContext(request))  # その他標準のコンテキスト
