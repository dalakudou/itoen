"""
WSGI config for itoen project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
import sys #add

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "itoen.settings") #del

sys.path.append('/opt/bitnami/apps/django/django_projects/itoen') #add
os.environ.setdefault("PYTHON_EGG_CACHE", "/opt/bitnami/apps/django/django_projects/itoen/egg_cache")  #add
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "itoen.settings")  #add

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
