# -*- coding: utf-8 -*-
from django.contrib import admin
from cms.models import ItemCatogory, Item, QuoteCategory, Quote, Fact



#マスタ系の登録
admin.site.register(ItemCatogory)
admin.site.register(QuoteCategory)


#項目の追加
admin.site.register(Item)

#ソースに対して事実を追加
class FactInline(admin.TabularInline):
    model = Fact
    extra = 3

#引用元の登録
class QuoteAdmin(admin.ModelAdmin):
	list_display = ('quoteName','quoteCategory','quoteUrl','publishDate')
	inlines = [FactInline,]

admin.site.register(Quote, QuoteAdmin)
