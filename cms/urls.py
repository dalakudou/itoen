# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from cms import views


urlpatterns = patterns('',

    url(r'^$', views.Search_view, name='search_result'),   # 検索用
    #url(r'^search/$', views.Search_all, name='search_all'),   # 全件出力　不要機能
    url(r'^search/detail/(?P<item_id>\d+)/$', views.Item_detail_view, name='item_detail'),  # 項目の詳細
)
