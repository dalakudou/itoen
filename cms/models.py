# -*- coding: utf-8 -*-
from django.db import models

#クラスの項目名は頭が小文字、テーブル名は大文字、単数と複数を意識したネーミングを
#related_nameは多い方を複数形にする


###   マスタ系     ######
class ItemCatogory(models.Model):
    '''項目のカテゴリ'''
    categoryName = models.CharField(u'項目カテゴリ', max_length=255,primary_key=True)

    class Meta:
        verbose_name = verbose_name_plural = u"1.項目カテゴリ(声優とか食べ物とか)"


    def __unicode__(self):    # Python2: def __unicode__(self):
        return self.categoryName


class QuoteCategory(models.Model):
    '''引用元マスタ'''
    quoteCategory = models.CharField(u'引用元カテゴリ', max_length=255,primary_key=True)   
    Category_Choices = (
    	(u'0',u'ブログ'),
    	(u'1',u'書籍'),
    	(u'2',u'ラジオ'),
    	(u'3',u'イベント'),
       	(u'4',u'その他'),
    	)
    category = models.CharField(max_length=1, choices=Category_Choices,default='0')

    class Meta:
        verbose_name = verbose_name_plural = u"2.引用元カテゴリ(ブログとか)"


    def __unicode__(self):    # Python2: def __unicode__(self):
        return self.quoteCategory

### 1:N  quote：fact
class Quote(models.Model):
    '''引用'''
    quoteName = models.CharField(u'引用元', max_length=255)
    quoteCategory = models.ForeignKey(QuoteCategory, verbose_name=u'引用元種類', related_name='quotecategory')
    quoteUrl = models.URLField((u'ソースURL'), null=True, blank=True)
    publishDate = models.DateField(u'発表された日付')

    class Meta:
        verbose_name = verbose_name_plural = u"3.引用元(ブログの記事とかラジオタイトルとか)"

    def __unicode__(self):    # Python2: def __unicode__(self):
        return self.quoteName


#fact　N:N　item
class Item(models.Model):
    '''項目'''
    itemName = models.CharField(u'項目名', max_length=255)
    itemNameKana = models.CharField(u'項目名カナ', max_length=255)
    itemCategory = models.ForeignKey(ItemCatogory, verbose_name=u'カテゴリ', related_name='itemcategories')
    itemOutline = models.TextField(u'概要', max_length=255,blank=True)

    class Meta:
        verbose_name = verbose_name_plural = u"5.項目登録"

    def __unicode__(self):    # Python2: def __unicode__(self):
        return self.itemName


class Fact(models.Model):
    '''事実'''
    quote = models.ForeignKey(Quote, verbose_name=u'出展元', related_name='facts')
    info = models.TextField(u'事実')
    aboutDate = models.DateField(u'ソース内で述べられている日付')
    In_date_Choices = (
        (u'0',u'確証有り'),
        (u'1',u'上旬頃'),
        (u'2',u'中旬頃'),
        (u'3',u'下旬頃'),
        (u'4',u'この日付と思われるが確証無し'),
        (u'9',u'日付不明'),
        )
    inDate = models.CharField(u'日付の精度', max_length=1, choices=In_date_Choices,default='0')
    items = models.ManyToManyField(Item)

    class Meta:
        verbose_name = verbose_name_plural = u"4.引用元に対して事実を登録"

    def __unicode__(self):    # Python2: def __unicode__(self):
        return self.info




#class FactTag(models.Model):
#    '''事実とアイテムのタグ'''
#    facttag = models.ForeignKey(Fact, verbose_name=u'事実', related_name='facttags')
#    itemtag = models.ForeignKey(Item, verbose_name=u'項目', related_name='itemtags')

#    def __int__(self):    # Python2: def __unicode__(self):
#        return self.id

