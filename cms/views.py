# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from cms.models import Item,Fact,Quote,ItemCatogory,QuoteCategory


def Search_all(request):
    #項目名が一致する項目を取得し、その項目から事実を取得、その事実を持つ項目を返す
    items = Item.objects.all().order_by('itemNameKana')
    return render_to_response('cms/search.html',  # 使用するテンプレート
                              {'items': items},       # テンプレートに渡すデータ
                              context_instance=RequestContext(request))  # その他標準のコンテキスト



def Search_view(request):
    '''検索結果の一覧'''
    #項目名が一致する項目を取得し、その項目から事実を取得、その事実を持つ項目を返す

    errors = []
    if request.method == 'GET':

        if 'word' in request.GET:
            word = request.GET['word']
            items = Item.objects.filter(itemName__icontains=word)
            return render_to_response('cms/search.html',  # 使用するテンプレート
                                     {'items': items},     # テンプレートに渡すデータ
                                     context_instance=RequestContext(request))  # その他標準のコンテキスト
        else:
            errors.append('何か入れてよ！')          
            return render_to_response(request, 'itoen/index.html',{'errors': errors})

                          
                              

def Item_detail_view(request, item_id=None,search_rslt=None):
    '''該当する事実を表示'''

    item = get_object_or_404(Item, pk=item_id)

    #多対多の逆参照(子→親)の場合はモデル名を小文字にして後ろに＿setをつける
    #アイテムの項目番号をキーに事実を取得する、この時点でfactとquote等関連するデータすべて持ってきてる
    facts = item.fact_set.all() 
    


    return render_to_response('cms/detail.html',
                              {'item': item,'facts':facts},
                              context_instance=RequestContext(request))


