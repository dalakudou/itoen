# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Fact',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('info', models.TextField(verbose_name='事実')),
                ('aboutDate', models.DateField(verbose_name='ソース内で述べられている日付')),
                ('inDate', models.CharField(max_length=1, verbose_name='日付の精度', default='0', choices=[('0', '確証有り'), ('1', '上旬頃'), ('2', '中旬頃'), ('3', '下旬頃'), ('4', 'この日付と思われるが確証無し'), ('9', '日付不明')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('itemName', models.CharField(max_length=255, verbose_name='項目名')),
                ('itemNameKana', models.CharField(max_length=255, verbose_name='項目名カナ')),
                ('itemOutline', models.TextField(max_length=255, verbose_name='概要', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemCatogory',
            fields=[
                ('categoryName', models.CharField(max_length=255, serialize=False, verbose_name='項目カテゴリ', primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('quoteName', models.CharField(max_length=255, verbose_name='引用元')),
                ('quoteUrl', models.URLField(null=True, verbose_name='ソースURL', blank=True)),
                ('publishDate', models.DateField(verbose_name='発表された日付')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='QuoteCategory',
            fields=[
                ('quoteCategory', models.CharField(max_length=255, serialize=False, verbose_name='引用元カテゴリ', primary_key=True)),
                ('category', models.CharField(max_length=1, default='0', choices=[('0', 'ブログ'), ('1', '書籍'), ('2', 'ラジオ'), ('3', 'イベント'), ('4', 'その他')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='quote',
            name='quoteCategory',
            field=models.ForeignKey(verbose_name='引用元種類', related_name='quotecategory', to='cms.QuoteCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='itemCategory',
            field=models.ForeignKey(verbose_name='カテゴリ', related_name='itemcategories', to='cms.ItemCatogory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fact',
            name='items',
            field=models.ManyToManyField(to='cms.Item'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fact',
            name='quote',
            field=models.ForeignKey(verbose_name='出展元', related_name='facts', to='cms.Quote'),
            preserve_default=True,
        ),
    ]
